# TicTactoe MinMax Alpha–beta pruning

## Introduction
The Repository contains a tic-tac-toe game with the following features:  
* MinMax algorithm with alpha-beta pruning used in computer turn.
* Configurable board size
* simple CLI and simple GUI, with separation UI from the backend.
* Recursion depth limitation with evaluation function(todo)

## Technologies
* C++17
* Gtest
* Cmake
* QT5

## Usage
```
mkdir build
cd build
cmake .. -DCMAKE_PREFIX_PATH=path to qt #prefix path needed only if gui will be compiled 

#specify a target to compile

make cli
make qtGUI
make tests
```
(todo) usage with docker

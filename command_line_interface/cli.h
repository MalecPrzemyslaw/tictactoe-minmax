#pragma once

#include <string>
#include <iostream>
#include <functional>

#include "Board.h"
#include "gameConfig.h"
#include "GameControler.h"
#include "WinChecker.h"

class CLI{
    GameControler& _controler;
    const Board& _board;
    std::string _rowDelimiter;
    public:
    CLI(GameControler& controler_,const Board& board_):_controler(controler_),_board(board_){
        _rowDelimiter=std::string(_board.getSize()*2+1,'-');
        _controler.registerCallbacks(std::bind(&CLI::EndOfGame,this,std::placeholders::_1),nullptr);//setFieldCallback is not used in CLI. CLI uses const ref to Board to draw game state.
    }
    void clearConsole(){
        #if defined _WIN32
            system("cls");
        #elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
            system("clear");
        #elif defined (__APPLE__)
            system("clear");
        #endif
}
    void drawCharacter(Symbol symbol){
        switch (symbol)
        {
        case Symbol::Circle :
            std::cout<<"O|";
            break;
        case Symbol::Cross :
            std::cout<<"X|";
            break;
        default:
            std::cout<<"*|";
            break;
        }
    }
    void drawBoard(){
        std::cout<<std::endl<<_rowDelimiter<<std::endl;
        for (size_t i = 0; i < _board.getSize(); i++)
        {
            std::cout<<"|";
            for (size_t j = 0; j < _board.getSize(); j++)
                {
                    drawCharacter(_board[i][j]);
                }
            std::cout<<std::endl<<_rowDelimiter<<std::endl;
        }
    }
    void takeInput(){
        size_t row,column;
        std::cout<<"Make next move!"<<std::endl;
        std::cout<<"Specify row"<<" range [0-"<<_board.getSize()<<")"<<std::endl;
        std::cin>>row;
        std::cout<<"Specify column"<<" range [0-"<<_board.getSize()<<")"<<std::endl;
        std::cin>>column;
        if(row>=_board.getSize()||column>=_board.getSize()){
            std::cout<<"You've specified illegal move! Press enter to try again"<<std::endl;
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            getchar();
            return;
        }
        auto retVal = _controler.makeNextMove(row,column);
        if(retVal==false){
            std::cout<<"You've specified illegal move! Press enter to try again"<<std::endl;
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            getchar();
        }
        std::cout<<std::endl;
    }
    void EndOfGame(Symbol winner){
        if(winner==PLAYER)
            std::cout<<"Player has won!";
        else if(winner==OPPONENT)
            std::cout<<"Opponet has won!";
        else
            std::cout<<"Draw";
        std::cout<<std::endl;
        exit(0);
    }
};

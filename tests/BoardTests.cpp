#include <gtest/gtest.h>
#include "Board.h"
TEST(Board, Board_Should_Init_Symbols){
    Board b(3);
    ASSERT_EQ(b[0][0],Symbol::None);
    ASSERT_EQ(b[0][1],Symbol::None);
    ASSERT_EQ(b[0][2],Symbol::None);
    ASSERT_EQ(b[2][2],Symbol::None);
    ASSERT_EQ(b[1][2],Symbol::None);
}
TEST(Board, BoardShouldHoldsSymbols){
    Board b(5);
    b[4][4]=Symbol::Circle;
    ASSERT_EQ(b[4][4],Symbol::Circle);
}
TEST(Board, BoardShouldCheckIfThereIsAnyMove1){
    Board b(2);
    b[0][0]=Symbol::Circle;
    b[0][1]=Symbol::Circle;
    b[1][0]=Symbol::Cross;
    b[1][1]=Symbol::Cross;
    ASSERT_EQ(b.isAnyMove(),false);
}
TEST(Board, BoardShouldCheckIfThereIsAnyMove2){
    Board b(2);
    b[0][0]=Symbol::Circle;
    b[0][1]=Symbol::Circle;
    b[1][1]=Symbol::Cross;
    ASSERT_EQ(b.isAnyMove(),true);
}
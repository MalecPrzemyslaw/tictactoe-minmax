#include <gtest/gtest.h>
#include "Board.h"
#include "WinChecker.h"

TEST(WinChecker, Board3ShouldCheckWininX){
    Board b(3);
    WinChecker wch(3);
    b[0][0]=Symbol::Circle;
    b[0][1]=Symbol::Circle;
    b[0][2]=Symbol::Circle;
    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,{2,0}),true);
}
TEST(WinChecker, Board5ShouldCheckWininX){
    Board b(5);
    b[0][0]=Symbol::Circle;
    b[0][1]=Symbol::Cross;
    b[0][2]=Symbol::Circle;
    b[0][3]=Symbol::Circle;
    b[0][4]=Symbol::Circle;
    WinChecker wch(3);

    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,{2,0}),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,{2,0}),false);
}

TEST(WinChecker, Board5ShouldCheckWininX2){
    Board b(5);
    b[0][0]=Symbol::Circle;
    b[0][1]=Symbol::Cross;
    b[0][2]=Symbol::Circle;
    b[0][3]=Symbol::Circle;
    WinChecker wch(2);

    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,{2,0}),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,{2,0}),false);
}

TEST(WinChecker, Board3ShouldCheckWininY){
    Board b(3);
    b[0][1]=Symbol::Cross;
    b[1][1]=Symbol::Cross;
    b[2][1]=Symbol::Cross;
    WinChecker wch(3);

    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,{1,2}),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,{1,2}),false);
}

TEST(WinChecker, Board5ShouldCheckWininY){
    Board b(5);
    b[0][0]=Symbol::Circle;
    b[1][2]=Symbol::Cross;
    b[2][3]=Symbol::Circle;
    b[3][3]=Symbol::Circle;
    b[4][3]=Symbol::Circle;
    WinChecker wch(3);

    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,{3,4}),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,{3,4}),false);
}

TEST(WinChecker, Board5ShouldCheckWininXY){
    Board b(5);
    b[0][0]=Symbol::Circle;
    b[1][1]=Symbol::Cross;
    b[2][2]=Symbol::Circle;
    b[3][3]=Symbol::Circle;
    b[4][4]=Symbol::Circle;
    WinChecker wch(3);

    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,{4,4}),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,{4,4}),false);
}
TEST(WinChecker, Board5ShouldCheckWininXY2){
    Board b(5);
    b[4][1]=Symbol::Circle;
    b[3][2]=Symbol::Circle;
    b[2][3]=Symbol::Circle;
    b[1][4]=Symbol::Circle;

    WinChecker wch(4);

    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,{4,1}),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,{4,1}),false);
}

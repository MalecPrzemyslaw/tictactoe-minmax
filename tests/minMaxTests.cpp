#include <gtest/gtest.h>
#include "Board.h"
#include "MinMax.h"

TEST(MinMax, Find_Win_Board_3_1){
    Board b(3);
    WinChecker wch(3);
    MinMaxAlg alg(Symbol::Circle,Symbol::Cross,wch);
    b[0][0]=Symbol::Circle;
    b[1][1]=Symbol::Circle;
    b[1][2]=Symbol::Cross;
    auto move = alg.minMax(b).second;
    b[move.row][move.column]=Symbol::Circle;
    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,move),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,move),false);
}

TEST(MinMax, Find_Win_Board_3_2){
    Board b(3);
    WinChecker wch(3);
    MinMaxAlg alg(Symbol::Circle,Symbol::Cross,wch);
    b[0][0]=Symbol::Circle;
    b[0][1]=Symbol::Cross;
    b[0][2]=Symbol::Cross;
    b[1][0]=Symbol::Cross;
    b[1][2]=Symbol::Circle;
    b[2][1]=Symbol::Cross;

    auto move = alg.minMax(b).second;
    b[move.row][move.column]=Symbol::Circle;
    b[2][0]=Symbol::Cross;
    move = alg.minMax(b).second;
    b[move.row][move.column]=Symbol::Circle;
    ASSERT_EQ(move.column,2);
    ASSERT_EQ(move.row,2);
    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,move),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,move),false);
}

TEST(MinMax, Find_Win_Board_3_3){
    Board b(3);
    WinChecker wch(3);
    MinMaxAlg alg(Symbol::Cross,Symbol::Circle,wch);
    b[0][0]=Symbol::Cross;
    b[0][1]=Symbol::Circle;
    b[0][2]=Symbol::Cross;
    b[1][0]=Symbol::Circle;
    b[1][2]=Symbol::Circle;
    b[2][1]=Symbol::Cross;
    auto move = alg.minMax(b).second;
    b[move.row][move.column]=Symbol::Cross;

    move = alg.minMax(b).second;
    b[move.row][move.column]=Symbol::Cross;
    ASSERT_EQ(move.column,0);
    ASSERT_EQ(move.row,2);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,move),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,move),false);
}

TEST(MinMax, Find_Win_Board4_1){
    Board b(4);
    WinChecker wch(4);
    MinMaxAlg alg(Symbol::Cross,Symbol::Circle,wch);
    b[0][0]=Symbol::Cross;
    b[0][1]=Symbol::Cross;
    b[1][3]=Symbol::Circle;
    b[2][1]=Symbol::Circle;
    b[3][3]=Symbol::Circle;
    b[3][2]=Symbol::Cross;
    auto move = alg.minMax(b).second;
    b[move.row][move.column]=Symbol::Cross;
    b[2][2]=Symbol::Circle;
    move = alg.minMax(b).second;
    b[move.row][move.column]=Symbol::Cross;
    ASSERT_EQ(move.column,3);
    ASSERT_EQ(move.row,0);
    ASSERT_EQ(wch.hasWon(b,Symbol::Cross,move),true);
    ASSERT_EQ(wch.hasWon(b,Symbol::Circle,move),false);
}

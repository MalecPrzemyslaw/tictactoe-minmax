#include "mainwindow.h"
#include <QApplication>
#include <QPushButton>
#include <QGridLayout>
#include <QMessageBox>

#include "MinMax.h"
#include "gameConfig.h"
#include "GameControler.h"
#include "WinChecker.h"
QPushButton *btn[M][M];
Board b(M);
WinChecker wch(K);
MinMaxAlg alg(OPPONENT,PLAYER,wch);
GameControler gc = GameControler(b,wch,alg);

void endGameCallback(Symbol player){
    QMessageBox msg;
    if(player==PLAYER)
        msg.setText("Player has won!");
    else if(player==OPPONENT)
        msg.setText("Opponet has won!");
    else
        msg.setText("Draw");
    msg.exec();
    return;

}
void setFieldCallback(Symbol player,Move move){
    if(player==Symbol::Circle)
        btn[move.row][move.column]->setText("O");
    else if(player==OPPONENT)
        btn[move.row][move.column]->setText("X");
}

QGridLayout* createBoardLayout(){
    QGridLayout *btnLayout = new QGridLayout();

    const QSize btnSize = QSize(50, 50);
        for(size_t i = 0; i < M; i++) {
            for(size_t j = 0; j < M; j++) {
                btn[i][j] = new QPushButton();
                btn[i][j]->setObjectName("");
                btn[i][j]->setFixedSize(btnSize);
                QAbstractButton::connect(btn[i][j], &QPushButton::clicked, [=](){
                    gc.makeNextMove(i, j);     // Call the function which uses i and j here
                });
                btnLayout->addWidget(btn[i][j], i, j);
                btnLayout->setSpacing(0);
            }
    }
    btnLayout->setSizeConstraint(QLayout::SetFixedSize);
    return btnLayout;
}
int main(int argc, char *argv[])
{
    gc.registerCallbacks(endGameCallback,setFieldCallback);
    QApplication a(argc, argv);

    // Create a widget
    QWidget *w = new QWidget();

    // Set the grid layout as a main layout
    w->setLayout(createBoardLayout());
    w->setWindowTitle("MNK game");
    w->show();
    return a.exec();
}

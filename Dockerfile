FROM ubuntu:20.04
RUN apt-get --allow-releaseinfo-change update
RUN apt-get update && apt-get upgrade -y
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN apt-get install -y qt5-default cmake build-essential valgrind
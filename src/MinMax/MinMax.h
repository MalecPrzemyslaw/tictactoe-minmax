#pragma once
#include "Board/Board.h"
#include "iostream"
#include "GameCommonTypes.h"
#include "WinChecker.h"

class IMinMaxAlg{
    public:
    static int constexpr WinVal=10000;
    virtual std::pair<int,Move> minMax(IBoard& board,size_t depth=0,bool isMax=true,int alpha=-WinVal,int beta=WinVal)=0;
};

class MinMaxAlg:public IMinMaxAlg{
    public:
    MinMaxAlg(Symbol aiPlayer_,Symbol opponent_,IWinChecker& wch_):aiPlayer(aiPlayer_),opponent(opponent_),wch(wch_){}
    Symbol aiPlayer ;
    Symbol opponent;//todo: overload negationOperator,make class?
    Move lastMove={};
    IWinChecker& wch;
    virtual std::pair<int,Move> minMax(IBoard& board,size_t depth=0,bool isMax=true,int alpha=-WinVal,int beta=WinVal) override;
};

#include "MinMax.h"
#include <algorithm>
std::pair<int,Move> MinMaxAlg::minMax(IBoard& board,size_t depth,bool isMax,int alpha,int beta){
    if(wch.hasWon(board,aiPlayer,lastMove))
         return {WinVal-depth,lastMove};
    if(wch.hasWon(board,opponent,lastMove))
         return {-WinVal+depth,lastMove};
    if (board.isAnyMove()==false)
         return {0,lastMove};
     Move bestMove;
     int best = isMax ? -INT32_MAX: INT32_MAX;
     for (size_t i = 0; i<board.getSize(); i++)
     {
         for (size_t j = 0; j<board.getSize(); j++)
         {
             if (board[i][j]==Symbol::None)
             {
                 board[i][j] = isMax ? aiPlayer: opponent;
                 lastMove.column=j;
                 lastMove.row=i;
                 auto retVal = minMax(board, depth+1, !isMax,alpha,beta);
                 if(isMax){
                     if(retVal.first>best){
                         best=retVal.first;
                         bestMove={j,i};
                         alpha = std::max(alpha, best);
                     }
                 }
                 else{
                     if(retVal.first<best){
                         best=retVal.first;
                         bestMove={j,i};
                         beta = std::min(beta, best);
                     }
                 }
                 board[i][j] = Symbol::None;
                 lastMove.column=j;
                 lastMove.row=i;
                 if (beta <= alpha)
                 {
                    return {best,bestMove};
                 }
             }
         }
     }
    return {best,bestMove};
}

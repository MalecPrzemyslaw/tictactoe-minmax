#include "GameControler.h"
GameControler::GameControler(IBoard& board,IWinChecker& wch,IMinMaxAlg& minMaxAlg):
    _board(board),_wch(wch),_minMaxAlg(minMaxAlg),isPlayerTurn(true){}

void GameControler::registerCallbacks(endGameCallback endClbk,setFieldCallback settedFeldClbk){
    if(!endClbk){
        _endGameCallback=[](Symbol){};
    }
    else{
    _endGameCallback = std::move(endClbk);
    }
    if(!settedFeldClbk){
        _setFieldCallback=[](Symbol, Move){};
    }
    else{
    _setFieldCallback = std::move(settedFeldClbk);
    }
}

bool GameControler::makeNextMove(size_t row, size_t column){
    if(isPlayerTurn==false)
        return false;
    if(_board[row][column]!=Symbol::None){
        return false;
    }
    isPlayerTurn=false;
    _board[row][column]=PLAYER;
    _setFieldCallback(PLAYER,{column,row});

    if(_wch.hasWon(_board,PLAYER,{column,row})){
        _endGameCallback(PLAYER);
        return true;
    }
    if(_board.isAnyMove()==false){
        _endGameCallback(Symbol::None);//Draw
        return true;
    }
    auto move = _minMaxAlg.minMax(_board,0,true).second;
    _setFieldCallback(OPPONENT,move);
    _board[move.row][move.column]=OPPONENT;

    if(_wch.hasWon(_board,OPPONENT,{move.column,move.row})){
        _endGameCallback(OPPONENT);
        return true;
    }
    if(_board.isAnyMove()==false){
        _endGameCallback(Symbol::None);//Draw
        return true;
    }
    isPlayerTurn=true;
    return true;
}
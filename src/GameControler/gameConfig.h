#pragma once
#include <stddef.h>
#include "Board.h"
#include "GameCommonTypes.h"
constexpr const size_t M=3;
constexpr const size_t K=3;
constexpr const Symbol PLAYER=Symbol::Circle;

constexpr Symbol OPPONENT = PLAYER==Symbol::Circle ? Symbol::Cross : Symbol::Circle;
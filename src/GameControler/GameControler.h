#pragma once
#include "Board.h"
#include <functional>
#include "gameConfig.h"
#include "GameCommonTypes.h"
#include "WinChecker.h"
#include "MinMax.h"

class GameControler{
    using endGameCallback = std::function<void(Symbol)>;
    using setFieldCallback = std::function<void(Symbol,Move)>;
    endGameCallback _endGameCallback;
    setFieldCallback _setFieldCallback;

    IBoard& _board;
    IWinChecker& _wch;
    IMinMaxAlg& _minMaxAlg;
    bool isPlayerTurn;

public:
    GameControler(IBoard& board,IWinChecker& wch,IMinMaxAlg& minMaxAlg);
    void registerCallbacks(endGameCallback,setFieldCallback);
    //return value true-legal move has been made false otherwise
    bool makeNextMove(size_t row, size_t column);
};
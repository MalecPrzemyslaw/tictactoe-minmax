#include "WinChecker.h"

WinChecker::WinChecker(const size_t crops2Win_): _crops2Win(crops2Win_){}

bool WinChecker::hasWonX(const Symbol player,const Move& lastMove)const{
    int XStart,XEnd,elementsInRow=0;
    SetBoundaries(XStart,XEnd,lastMove.column);
    for (size_t i = XStart; i <= XEnd; i++)
    {
        elementsInRow = (*_ptrBoard)[lastMove.row][i]==player ? ++elementsInRow : 0;
        if(elementsInRow>=_crops2Win)
            return true;
    }
    return false;
}
bool WinChecker::hasWon(const IBoard& board,const Symbol player,const Move& lastMove){
    _ptrBoard = &board;
    if(hasWonX(player,lastMove))
        return true;
    if(hasWonY(player,lastMove))
        return true;
    if(hasWonXY1(player,lastMove))
        return true;
    if(hasWonXY2(player,lastMove))
        return true;
    return false;
}

bool WinChecker::hasWonY(const Symbol player,const Move& lastMove)const{
    int Start,End,elementsInRow=0;
    SetBoundaries(Start,End,lastMove.row);
    for (size_t i = Start; i <= End; i++)
    {
        elementsInRow = (*_ptrBoard)[i][lastMove.column]==player ? ++elementsInRow : 0;
        if(elementsInRow>=_crops2Win)
            return true;
    }
    return false;
}

bool WinChecker::hasWonXY1(const Symbol player,const Move& lastMove)const{
    int StartX,EndX,StartY,EndY,elementsInRow=0;
    SetBoundariesXY1(StartX,StartY,EndX,EndY,lastMove);
    {
        int i,j;
        for (i = StartX,j = StartY; i <= EndX && j<=EndY; i++,j++)
        {
            elementsInRow = (*_ptrBoard)[j][i]==player ? ++elementsInRow : 0;
            if(elementsInRow>=_crops2Win)
                return true;
        }
    }
    return false;
}

bool WinChecker::hasWonXY2(const Symbol player,const Move& lastMove)const{
    int StartX,EndX,StartY,EndY,elementsInRow=0;
    SetBoundariesXY2(StartX,StartY,EndX,EndY,lastMove);
    {
        int i,j;
        for (i = StartX,j = EndY; i <= EndX && j>=StartY; i++,j--)
        {
            elementsInRow = (*_ptrBoard)[j][i]==player ? ++elementsInRow : 0;
            if(elementsInRow>=_crops2Win)
                return true;
        }
    }
    return false;
}

void WinChecker::SetBoundaries(int& Begin,int& End,size_t StartingPoint)const{
    Begin = StartingPoint-_crops2Win+1;
    End = StartingPoint+_crops2Win-1;
    if(Begin<0)
        Begin=0;
    if(End>=_ptrBoard->getSize())
        End=_ptrBoard->getSize()-1;
}

void WinChecker::SetBoundariesXY1(int& BeginX,int& BeginY,int& EndX,int& EndY,Move StartingPoint)const{
    BeginX = StartingPoint.column-_crops2Win+1;
    BeginY = StartingPoint.row-_crops2Win+1;
    EndX = StartingPoint.column+_crops2Win-1;
    EndY = StartingPoint.row+_crops2Win-1;
    if(BeginX<0||BeginY<0){
        int min = std::min(BeginX, BeginY);
        BeginX-=min;
        BeginY-=min;
    }
    if(EndX>=_ptrBoard->getSize()||EndY>=_ptrBoard->getSize()){
        int max = std::max(EndX, EndY);
        max =max-_ptrBoard->getSize()+1;
        EndX-=max;
        EndY-=max;
    }
}

void WinChecker::SetBoundariesXY2(int& BeginX,int& BeginY,int& EndX,int& EndY,Move StartingPoint) const{
    BeginX = StartingPoint.column-_crops2Win+1;
    BeginY = StartingPoint.row-_crops2Win+1;
    EndX = StartingPoint.column+_crops2Win-1;
    EndY = StartingPoint.row+_crops2Win-1;
    int YEndDistance = _ptrBoard->getSize()-EndY-1;
    int XEndDistance = _ptrBoard->getSize()-EndX-1;
    if(BeginX<0||YEndDistance<0){
        int min = std::min(BeginX, YEndDistance);
        BeginX-=min;
        EndY+=min;
    }
    if(BeginY<0||XEndDistance<0){
        int min = std::min(BeginY, XEndDistance);
        BeginY-=min;
        EndX+=min;
    }
}
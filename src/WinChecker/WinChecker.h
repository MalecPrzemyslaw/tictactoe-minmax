#pragma once
#include "GameCommonTypes.h"
#include "Board.h"

class IWinChecker{
    public:
    virtual bool hasWon(const IBoard& board_,const Symbol player,const Move& lastMove)=0;
};

class WinChecker: public IWinChecker{
public:
    WinChecker(const size_t Crops2Win);
    virtual bool hasWon(const IBoard& board,const Symbol player,const Move& lastMove) override ;
private:
    const size_t _crops2Win;
    const IBoard* _ptrBoard;
    bool hasWonX(const Symbol player,const Move& lastMove)const;
    bool hasWonY(const Symbol player,const Move& lastMove)const;
    bool hasWonXY1(const Symbol player,const Move& lastMove)const;
    bool hasWonXY2(const Symbol player,const Move& lastMove)const;
    void SetBoundaries(int& Begin,int& End,size_t StartingPoint)const;
    void SetBoundariesXY1(int& BeginX,int& BeginY,int& EndX,int& EndY,Move StartingPoint)const;
    void SetBoundariesXY2(int& BeginX,int& BeginY,int& EndX,int& EndY,Move StartingPoint)const;
};
#pragma once
#include <stddef.h>

enum class Symbol{
    None,
    Cross,
    Circle
};
struct Move{
    size_t column;
    size_t row;
};
#pragma once
#include <algorithm>
#include <vector>
#include "GameCommonTypes.h"

class IBoard{
    public:
    using Row = std::vector<Symbol>;
    using BoardData = std::vector<Row>;

    virtual bool isAnyMove() const=0;
    virtual Row& operator[](int idx)=0;
    virtual const Row& operator[](int idx)const=0;
    virtual size_t getSize() const=0;
};
class Board: public IBoard{
    public:
    explicit Board(int nbOfRows);
    bool isAnyMove() const override;
    inline Row& operator[](int idx) override{
        return board_[idx];
    }
    inline const Row& operator[](int idx) const override{
        return board_[idx];
    }
    size_t getSize() const override;
    private:
    BoardData board_;
};
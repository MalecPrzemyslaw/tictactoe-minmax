#include "Board.h"
#include <algorithm>
size_t Board::getSize() const{
    return board_.size();
}

bool Board::isAnyMove() const{
    return std::any_of(board_.cbegin(),board_.cend(),[](Row row){
        return std::any_of(row.cbegin(),row.cend(),[](Symbol sym){
            return sym==Symbol::None;
        });
    });
}

Board::Board(int nbOfRows):board_(nbOfRows,Row(nbOfRows,Symbol::None)){
}
